# Nagios Plugins

A collection of nagios compatible plugins I wrote for monitoring services. I use these with LibreNMS mostly.

File names follow the convention `check_<service>` this is so Nagios or LibreNMS can detect it.

Response messages of the various scripts follow the Nagios documented format: [AEN200](https://nagios-plugins.org/doc/guidelines.html#AEN200)

For my fork of a Minecraft monitoring plugin which graphs player count see: [nagios-plugin-minecraft](https://github.com/2E0PGS/nagios-plugin-minecraft)